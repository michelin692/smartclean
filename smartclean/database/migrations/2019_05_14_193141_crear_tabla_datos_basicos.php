<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDatosBasicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dato_basico', function (Blueprint $table) {

            $table->increments('id_dato_basico');
            $table->string("nombre",32);
            $table->string("apellido",32);
            $table->string("sexo",10);
            $table->date("fecha_nac");
            $table->string("documento_identidad",11);
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dato_basico');
    }
}
