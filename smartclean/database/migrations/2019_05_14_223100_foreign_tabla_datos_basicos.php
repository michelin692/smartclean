<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class ForeignTablaDatosBasicos extends Migration
{
  public function up()
  {
    Schema::table('dato_basico', function (Blueprint $table) {
      $table->foreign('usuario_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
    });
  }

}
?>
