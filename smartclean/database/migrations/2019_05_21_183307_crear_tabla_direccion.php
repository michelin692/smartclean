<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDireccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('direccion', function (Blueprint $table) {

            $table->increments('direccion_id');
            $table->enum('tipo', ['avenida', 'calle']);
            $table->string('poblacion',32);
            $table->string('codigo_postal',16);
            $table->string('pais',16);
            $table->timestamp('fecha_alta')->nullable();
            $table->timestamp('fecha_mod')->nullable();
            $table->timestamp('fecha_baja')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
