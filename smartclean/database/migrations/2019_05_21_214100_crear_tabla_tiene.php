<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTiene extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tiene', function (Blueprint $table) {

            $table->integer('cliente_id')->unsigned();
            $table->integer('direccion_id')->unsigned();
            $table->integer('trabajador_id')->unsigned();
            $table->timestamp('fecha_alta')->nullable();
            $table->timestamp('fecha_mod')->nullable();
            $table->timestamp('fecha_baja')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
